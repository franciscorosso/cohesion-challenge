# Cohesion Challenge - Francisco Rosso

- [Cohesion - Challenge Rosso](#challenge-rosso)
  - [SetUp](#set-up)
  - [Fake API](#fake-api)
  - [Location Simulate](#location-simulate)
  - [Test case](#test-case)
  - [Crashlytics](#crashlytics)

  
---

## SetUp
Clone the repo and open it on XCode.  No ```pod install``` needed.
Grant permissions and **ALLOW ONCE** Location Services when app requests it. The app will run only on iOS 15 devices.  

The app shows whether the user is inside or outside the region (white label) AND shows when the CLLocationManager monitor function detects the user has crossed the border of the region (orange label). The CLLocationManager region monitoring function (the one that triggers didEnterRegion/didExitRegion callbacks) is not as precise as it should be, so maybe a new logic could be implemented (for example, store the previous location and compare it with the new one). 
##

## Fake API
The API used to send the region events (enter/exit) is faked by using CoreData framework. 
##

## Simulate Location
```Villa Maria - TestWP.gpx``` file was added to simulate some waypoints to enter/exit the hardcoded region. 
In order to change this waypoints, you can either add a new .gpx file or open the one in the project and modify waypoint's latitude and/or longitude.

To run the .gpx file, first run the app either on a Simulator or a real device, then enable it:


```
Xcode > Debug > Simulate Location > Villa Maria - TestWP
```

##

## Test case
One test case was added to the project in order to evaluate that when the region is modified by the user, the app monitors the new region instead the old one.  
##

## Crashlytics
Not implemented as I was running out of time. 
##
