//
//  MockLocationManager.swift
//  CohesionChallengeRossoTests
//
//  Created by Francisco Rosso on 20/01/2022.
//

import Foundation
import CoreLocation

@testable import CohesionChallengeRosso

class MockLocationManager: LocationManagerInterface {
        
    var locationManagerDelegate: LocationManagerDelegateInterface?
    
    var location: CLLocation?
    
    var desiredAccuracy: CLLocationAccuracy = kCLLocationAccuracyBestForNavigation
    
    func startUpdatingLocation() {
        
    }
    
    func startMonitoring(for region: CLRegion) {
        locationManagerDelegate?.locationFetcher(self, didStartMonitoringFor: region)
    }
    
    func stopMonitoring(for: CLRegion) {
        
    }
    
    func requestLocation() {
        
    }
    
    func requestAlwaysAuthorization() {
        
    }
    
    func requestWhenInUseAuthorization() {
        
    }
    
}

