//
//  CohesionChallengeRossoTests.swift
//  CohesionChallengeRossoTests
//
//  Created by Francisco Rosso on 18/01/2022.
//

import XCTest
import CoreLocation
@testable import CohesionChallengeRosso

class CohesionChallengeRossoTests: XCTestCase {

    /**
     This test case evaluates that region is being modified correctly on LocationManagerService and also that when we modify the region, the new one is being monitored.
     */
    func test_ModifyRegionMonitoringSuccess() throws {
        let mockLocationManager = MockLocationManager()
        let expectation = expectation(description: "Start monitoring expectation")
        expectation.expectedFulfillmentCount = 2
        
        let oldCoordinate = CLLocationCoordinate2D(latitude: -32.41501234807468, longitude: -63.2388244640439)
        let newCoordinate = CLLocationCoordinate2D(latitude: -25.01101244807468, longitude: -53.2388244640439)
        
        let newRegion = CLCircularRegion(center: newCoordinate,
                                         radius: 100.0, identifier: "geofence_mock_2")
        
        var currentRegion: CLRegion?
        
        let locationManagerService = LocationManagerService(locationManager: mockLocationManager) { region in
            currentRegion = region
            expectation.fulfill()
        }
        
        locationManagerService.monitorRegionAtLocation(center: oldCoordinate, identifier: "geofence_mock_1")
        
        XCTAssertEqual(oldCoordinate, locationManagerService.region.center)
        
        locationManagerService.monitorRegionAtLocation(center: newCoordinate, identifier: "geofence_mock_2")
        
        XCTAssertEqual(newCoordinate, locationManagerService.region.center)
        
        wait(for: [expectation], timeout: 5.0)
        
        XCTAssertEqual(newRegion.identifier, currentRegion?.identifier)

    }

}


