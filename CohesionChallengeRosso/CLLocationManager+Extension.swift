//
//  CLLocationManager+Extension.swift
//  CohesionChallengeRosso
//
//  Created by Francisco Rosso on 20/01/2022.
//

import CoreLocation

extension CLLocationManager: LocationManagerInterface {
    var locationManagerDelegate: LocationManagerDelegateInterface? {
        get { return delegate as! LocationManagerDelegateInterface? }
        set { delegate = newValue as! CLLocationManagerDelegate? }
    }
}

extension CLLocationCoordinate2D: Equatable {}

public func ==(lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
    return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
}
