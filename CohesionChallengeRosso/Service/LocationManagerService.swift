//
//  LocationManagerService.swift
//  CohesionChallengeRosso
//
//  Created by Francisco Rosso on 19/01/2022.
//

import CoreLocation

class LocationManagerService: NSObject {
    
    private static let GEOFENCE_IDENTIFIER: String = "geofence"
    
    static let shared = LocationManagerService()
    
    private var locationManager: LocationManagerInterface
    var region: CLCircularRegion!
    private static let MAX_DISTANCE: Double = 100.0
    private var onNewEvent: (RegionEvent) -> Void = { _ in }
    private var onError: () -> Void = { }
    private var onStartMonitoring: (CLRegion) -> Void
    
    init(locationManager: LocationManagerInterface = CLLocationManager(), onStartMonitoring: @escaping (CLRegion) -> Void = {_ in}) {

        self.locationManager = locationManager
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        self.onStartMonitoring = onStartMonitoring
        
        super.init()
        self.locationManager.locationManagerDelegate = self
    }
    
    func startMonitoring(onNewEvent: @escaping (RegionEvent) -> Void, onError: @escaping () -> Void) {
        self.onNewEvent = onNewEvent
        self.onError = onError
        
        startUpdatingLocation()
    }
    
    func startUpdatingLocation() {
        if CLLocationManager.locationServicesEnabled() {
            monitorRegionAtLocation(center: CLLocationCoordinate2D(latitude: -32.41501234807468, longitude: -63.2388244640439), identifier: LocationManagerService.GEOFENCE_IDENTIFIER)
            locationManager.startUpdatingLocation()
        } else {
            onError()
        }
    }
    
    func monitorRegionAtLocation(center: CLLocationCoordinate2D, identifier: String ) {
        if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            let maxDistance = LocationManagerService.MAX_DISTANCE
            region = CLCircularRegion(center: center,
                                      radius: maxDistance, identifier: identifier)
            region.notifyOnEntry = true
            region.notifyOnExit = true
            
            locationManager.startMonitoring(for: region)
        }
    }
    
    func modifyRegion(newLatitude: Double, newLongitude: Double) {
        locationManager.stopMonitoring(for: region)
        self.monitorRegionAtLocation(center: CLLocationCoordinate2D(latitude: newLatitude, longitude: newLongitude), identifier: LocationManagerService.GEOFENCE_IDENTIFIER)
    }
}

extension LocationManagerService: LocationManagerDelegateInterface {
    func locationFetcher(_ manager: LocationManagerInterface, didUpdateLocations locations: [CLLocation]) {
        guard let coordinate: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        print("Coordinate >>>>> \(coordinate.latitude) \(coordinate.longitude)")
        
        onNewEvent(RegionEvent(
            region: self.region,
            coordinate: coordinate,
            locationEvent: region.contains(coordinate) ? .insideRegion : .outsideRegion)
        )
    }
    
    func locationFetcher(_ manager: LocationManagerInterface, didEnterRegion region: CLRegion) {
        guard let coordinate: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        onNewEvent(RegionEvent(
            region: self.region,
            coordinate: coordinate,
            locationEvent: .didEnterRegion)
        )
    }
    
    func locationFetcher(_ manager: LocationManagerInterface, didExitRegion region: CLRegion) {
        guard let coordinate: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        onNewEvent(RegionEvent(
            region: self.region,
            coordinate: coordinate,
            locationEvent: .didExitRegion)
        )
    }
    
    func locationFetcher(_ manager: LocationManagerInterface, didStartMonitoringFor region: CLRegion) {
        self.onStartMonitoring(region)
    }
}

extension LocationManagerService: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationFetcher(manager, didUpdateLocations: locations)
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        self.locationFetcher(manager, didEnterRegion: region)
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        self.locationFetcher(manager, didExitRegion: region)
    }
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        self.locationFetcher(manager, didStartMonitoringFor: region)
    }
}
