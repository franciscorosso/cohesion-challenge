//
//  FakeAPIService.swift
//  CohesionChallengeRosso
//
//  Created by Francisco Rosso on 20/01/2022.
//

import UIKit
import CoreData

class FakeAPIService {
    
    static let shared = FakeAPIService()
    let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func getFromAPI() -> [Event] {
        let fetchRequest: NSFetchRequest<Event> = Event.fetchRequest()
        fetchRequest.returnsObjectsAsFaults = false
        var events = [Event]()
        
        do {
            let results = try managedContext.fetch(fetchRequest)
            events = results as [Event]
         } catch let error as NSError {
            print("No ha sido posible cargar \(error), \(error.userInfo)")
         }
        
        return events
    }
    
    func sendToAPI(regionEvent: RegionEvent) {
        let entity = NSEntityDescription.entity(forEntityName: "Event", in: managedContext)
        let event = NSManagedObject(entity: entity!, insertInto: managedContext)
        
        event.setValue(regionEvent.locationEvent == .didEnterRegion, forKey: "entryExitEvent")
        event.setValue(regionEvent.coordinate.latitude, forKey: "latitude")
        event.setValue(regionEvent.coordinate.longitude, forKey: "longitude")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Error while saving \(error), \(error.userInfo)")
        }
    }
    
}
