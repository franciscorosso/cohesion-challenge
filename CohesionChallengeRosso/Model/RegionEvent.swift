//
//  RegionEvent.swift
//  CohesionChallengeRosso
//
//  Created by Francisco Rosso on 19/01/2022.
//

import Foundation
import CoreLocation

struct RegionEvent {
    
    let region: CLCircularRegion
    let coordinate: CLLocationCoordinate2D
    let locationEvent: LocationEvent
    
}

enum LocationEvent {
    case didEnterRegion
    case didExitRegion
    case insideRegion
    case outsideRegion
}
