//
//  ViewController.swift
//  CohesionChallengeRosso
//
//  Created by Francisco Rosso on 18/01/2022.
//

import UIKit
import CoreLocation

class MainViewController: UIViewController {
    
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var inoutLabel: UILabel!
    
    let locationManagerService: LocationManagerService = LocationManagerService.shared
    let fakeAPIService: FakeAPIService = FakeAPIService.shared
    
    
    override func viewDidLoad() { }
    
    func fetchLocation() {
        locationManagerService.startMonitoring { event in
            
            switch event.locationEvent {
            case .insideRegion:
                self.infoLabel.text = "You are inside the region."
            case .outsideRegion:
                self.infoLabel.text = "You are outside the region."
            case .didEnterRegion:
                self.inoutLabel.text = "You have entered into the region."
                self.fakeAPIService.sendToAPI(regionEvent: event)
            case .didExitRegion:
                self.inoutLabel.text = "You have exit the region."
                self.fakeAPIService.sendToAPI(regionEvent: event)
            }
        } onError: {
            self.infoLabel.text = "Please enable location services and try again..."
        }
    }
    
    
}

