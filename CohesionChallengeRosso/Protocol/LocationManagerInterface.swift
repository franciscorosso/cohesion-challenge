//
//  LocationManagerInterface.swift
//  CohesionChallengeRosso
//
//  Created by Francisco Rosso on 20/01/2022.
//

import Foundation
import CoreLocation

protocol LocationManagerInterface {
    var locationManagerDelegate: LocationManagerDelegateInterface? { get set }
    var location: CLLocation? { get }
    var desiredAccuracy: CLLocationAccuracy { get set }
    
    func startUpdatingLocation()
    func startMonitoring(for: CLRegion)
    func stopMonitoring(for: CLRegion)
    func requestLocation()
    func requestAlwaysAuthorization()
    func requestWhenInUseAuthorization()
}

