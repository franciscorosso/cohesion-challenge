//
//  LocationManagerDelegate.swift
//  CohesionChallengeRosso
//
//  Created by Francisco Rosso on 20/01/2022.
//

import Foundation
import CoreLocation

protocol LocationManagerDelegateInterface: class {
    func locationFetcher(_ manager: LocationManagerInterface, didUpdateLocations locations: [CLLocation])
    func locationFetcher(_ manager: LocationManagerInterface, didEnterRegion region: CLRegion)
    func locationFetcher(_ manager: LocationManagerInterface, didExitRegion region: CLRegion)
    func locationFetcher(_ manager: LocationManagerInterface, didStartMonitoringFor region: CLRegion)
}
